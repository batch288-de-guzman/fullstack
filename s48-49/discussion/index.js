console.log(`sicko mode`);

fetch(`https://jsonplaceholder.typicode.com/posts`)
.then(result=> result.json())
.then(response =>{
    console.log(response)
    showPosts(response)
})

const showPosts = (posts) => {
    console.log(typeof posts)

    let entries =``

    posts.forEach((post) => {

        entries += `
        <div id = "post-${post.id}">
            <h3 id = "post-title-${post.id}">${post.title}</h3>
            <p id = "post-body-${post.id}">${post.body}</p>

            <button onclick = "editPost(${post.id})">Edit</button>

            <button onclick="deletePost(${post.id})">Delete</button>
        <div>
        `


        
    });

    document.querySelector(`#div-post-entries`).innerHTML = entries;
    console.log(document.querySelector(`#div-post-entries`))
}


//post data on the api

document.querySelector(`#form-add-post`).addEventListener(`submit`, (event)=>{
    event.preventDefault();

    fetch(`https://jsonplaceholder.typicode.com/posts`, {
        method: `POST`,
        headers: {
            'Content-type' : 'application/json'
        },
        body : JSON.stringify({
            title: document.querySelector('#txt-title').value,
            body:document.querySelector('#txt-body').value,
            userId:1
        })
    })
    .then(response => response.json())
    .then(result =>{
        console.log(result)

        alert(`post added successfully`)

        document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
    })

})

const editPost = (id) =>{

    let title = document.querySelector(`#post-title-${id}`).innerHTML
    let body = document.querySelector(`#post-body-${id}`).innerHTML


    document.querySelector(`#txt-edit-title`).value = title;
    document.querySelector(`#txt-edit-body`).value = body;
    document.querySelector(`#txt-edit-id`).value = id;


    //romoves disabled attribute once clicked 'edit'
    document.querySelector(`#btn-submit-update`).removeAttribute(`disabled`);
}

document.querySelector(`#form-edit-post`).addEventListener(`submit`,(event)=>{
    //prevent the automatic reload
    event.preventDefault();
    let id = document.querySelector(`#txt-edit-id`).value;

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
        method : `PUT`,
        headers: {
            'Content-type' : 'application/json'
        },
        body : JSON.stringify({
            title: document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value,
            userId:1
        })
    })
    .then(response => response.json())
    .then(result =>{
        console.log(result)

        alert(`post updated`);

        document.querySelector(`#post-title-${id}`).innerHTML = document.querySelector('#txt-edit-title').value

        document.querySelector(`#post-body-${id}`).innerHTML = document.querySelector('#txt-edit-body').value

        document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
        document.querySelector('#btn-submit-update').setAttribute(`disabled`, true);

    })

})


const deletePost = (id) => {
    // Send a DELETE request to the API endpoint for deleting the post
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
      method: 'DELETE'
    })
      .then(response => response.json())
      .then(result => {

        document.querySelector(`#post-${id}`).remove();

        alert(`post deleted`);
      });
  };

