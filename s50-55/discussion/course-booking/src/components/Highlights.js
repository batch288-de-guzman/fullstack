import { Container, Row, Col, Card } from "react-bootstrap"

export default function Highlights(){
    return(
        <>
            <Container className="mt-5">
                <Row>
                    <Col className="col-12 col-sm-12 col-md-4 g-2">
                        <Card className="cardHighlight">
                            <Card.Body>
                                <Card.Title>Learn from Home</Card.Title>
                                <Card.Text>
                                Some quick example text to build on the card title and make up the
                                bulk of the card's content.
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col className="col-12 col-sm-12 col-md-4 g-2">
                        <Card className="cardHighlight">
                            <Card.Body>
                                <Card.Title>Study Now Pay Later</Card.Title>
                                <Card.Text>
                                Some quick example text to build on the card title and make up the
                                bulk of the card's content.
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col className="col-12 col-sm-12 col-md-4 g-2">
                        <Card className="cardHighlight">
                            <Card.Body>
                                <Card.Title>Be Part of our Community</Card.Title>
                                <Card.Text>
                                Some quick example text to build on the card title and make up the
                                bulk of the card's content.
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>
    )
}