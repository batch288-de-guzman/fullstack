import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link, NavLink } from 'react-router-dom';



//user link and navlink to add hyperlink to our application, navlink for navigation bar

//the 'as' allows components to be treated as if they are a different component gaining the access to its properties and functionalities 

import { useContext } from 'react';
import UserContext from '../UserContext';


export default function AppNavBar(){

  // const [user, setUser] = useState(localStorage.getItem('email'));

  //consume the global context instead
  const {user} = useContext(UserContext)

    return(

      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand as = {Link} to = '/'>Zuitt</Navbar.Brand>
          <Nav className="ms-auto">
            <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
            <Nav.Link as = {NavLink} to = '/courses'>Courses</Nav.Link>
            {
	            	user.id === null

	            	?

	            	<>
	            		<Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>
	            		<Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
	            	</> 
	            	
	            	:

	            	<Nav.Link as = {NavLink} to = '/logout' >Logout</Nav.Link>
	            }
          </Nav>
        </Container>
      </Navbar>

    )
}