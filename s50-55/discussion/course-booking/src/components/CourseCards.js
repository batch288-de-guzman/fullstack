import { Card, Container ,Row, Col,Button } from "react-bootstrap";
import { Link } from 'react-router-dom';


//import the useState hook from react
import { useState, useEffect,useContext } from "react";
import UserContext from "../UserContext";

export default function CourseCards(props){
    console.log(props.courseProp);

    const {user} = useContext(UserContext)

    const {_id, name, description, price} = props.courseProp;

    //user the state hook for this component to be able to store its state
    //states are used to keep track of information related to individual components
        //synstax cons[getter,setter] = useState(initialGetterValue)

    const [count, setCount] = useState(0)
    const [isDisabled, setIsDisabled] = useState(false)
    const [seat, setSeat] = useState(30)


    function enroll(){
        if(seat === 0){
            alert(`No more seats available`)
            
        }else{
            setCount(count+1)
            setSeat(seat-1)
        }

    }


    //the function or side effect in the useeffect hook will invoke or run in the initial loading of the application and when there are changes on the dependencies

    //syntax: useEffect(side efecct, [dependencies])
    useEffect(()=>{
        if(seat === 0){
            setIsDisabled(true)
        }
    },[seat]);


    

    return(
        <>
            <Container className="mt-5 mb-5">
                <Row>
                    <Col className="g-2">
                   
                        <Card>
                            <Card.Body>
                                <Card.Title>{name}</Card.Title>
                                <h6>Description:</h6>
                                <Card.Text>
                                {description}
                                </Card.Text>
                                <h6>Prize:</h6>
                                <Card.Text>PhP 
                                {price}
                                </Card.Text>
                                <h6>Enrollees:</h6>
                                <Card.Text>{count}
                                </Card.Text>
                                {
                                    user !== null ?
                                    <Button as = {Link} to = {`/courses/${_id}`}>Course Info</Button>
                                    :
                                    
                                    <Button as = {Link} to = '/login'>Login to Enroll</Button>
                                }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>

            </Container>
        </>
    );
}