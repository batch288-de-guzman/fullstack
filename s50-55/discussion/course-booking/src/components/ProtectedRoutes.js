import { Route, redirect } from "react-router-dom";
import UserContext from "../UserContext";
import React from "react";

const useAuth = () => {
    const user = {loggedIn: false}
    return user && user.loggedIn    
}

const ProtectedRoutes = () =>{
    const isAuth = useAuth()
    
    return<div></div>
}


export default ProtectedRoutes;
