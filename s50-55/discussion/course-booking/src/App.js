import './App.css';

import { useEffect, useState } from 'react';

import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Page404 from './pages/Page404';
import CourseView from './pages/CourseView';

  /* The following line can be included in your src/index.js or App.js file */

import 'bootstrap/dist/css/bootstrap.min.css';

import { UserProvider } from './UserContext';


//the browser router component will enable simulate page navigation by syncing the shown content and the shown url in the web browser
//the routes components hold all the route components. it selects which route component show based ont the url endpoint
import { BrowserRouter, Route, Routes } from 'react-router-dom';


//Mounting - call components inside functions
function App() {

  /*if(localStorage.getItem('token')===null){
    
  }*/

  
  let userDetails = {};

  useEffect(()=>{
    if(localStorage.getItem('token') === null){
      userDetails = {
        id: null,
        isAdmin: null
      }
    }else{
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
          method: 'GET',
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(result => result.json())
      .then(data => {
          userDetails = {
              id : data._id,
              isAdmin: data.isAdmin
          };

      })
    }
  }, [])

  const [user, setUser] = useState(userDetails);

  const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(()=>{
  },[user])

  return (

    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavBar />
        <Routes>
          <Route path = '/' element = {<Home/>} />
          <Route path = '/courses' element = {<Courses />} />
          <Route path = '/register' element = {<Register />} />
          <Route path = '/login' element = {<Login />} />
          <Route path = '/logout' element = {<Logout />} />
          <Route path = '/courses/:courseId' element = {<CourseView />} />
          <Route path = '/*' element = {<Page404 />} />
        </Routes>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
