import React from "react";

//create a context object
//a context is a data that can be userd to store information that can be shared to other components within  the app
//context is a different approact to passing information bwtween components and allows easier access by avoiding the use of prop passing

//with the help of createContext() method where we are able to so create a context stored in a variable named UserContext

const UserContext = React.createContext();


//the 'provider' components allows other components to use the context object and supply necessary information needed to the context
export const UserProvider = UserContext.Provider;

export default UserContext;