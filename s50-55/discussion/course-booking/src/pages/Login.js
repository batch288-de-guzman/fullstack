import { Container, Row, Col, Button, Form } from "react-bootstrap"
import { useState, useEffect, useContext } from "react"
import { Navigate, Link, useNavigate  } from "react-router-dom";

import UserContext from "../UserContext";
import Swal2 from "sweetalert2";

export default function Login(){

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState ('');

    const {user, setUser} = useContext(UserContext);

    const retrieveUserDetails = (token) =>{
        fetch(`${process.env.REACT_APP_API_URL}/user/userDetails`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(result=> result.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })

            console.log(user)
        })
    }

    //local item method gets the value of the specified key from out local storage 
    // const [user, setUser] = useState(localStorage.getItem('email'));

    const [isDisabled, setIsDisabled] = useState(true)
    const navigate = useNavigate();

    useEffect(()=>{
        if(email !== '' && password !== ''){
            setIsDisabled(false)
        }else{
            setIsDisabled(true)
        }
    },[email, password]);

    const login =(event)=>{
        event.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/user/login`,{
            method: 'POST',
            headers:{
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email:email,
                password: password
            })
        }).then(result => result.json())
        .then(data =>{
            if(data === false){
                Swal2.fire({
                    "title":"login failed",
                    icon: "error",
                    text: "Check log in details and try again"

            })
            }else{
                localStorage.setItem('token', data.auth);
                retrieveUserDetails(data.auth);

                Swal2.fire({
                    "title":"login successful",
                    icon: "success",
                    text: "Welcome to Zuitt"

            })
                navigate('/')
            }
        })


        //set email of the authenticated user in the local storage
        //syntax: localStorage.setItem('propertyName')

        // localStorage.setItem('email', email);
        // setUser(localStorage.getItem('email'));



    }

    return(
        (user.id === null) ?
            <Container>
                <Row>
                    <Col className="col-lg-6 mx-auto">
                    <h1 className="text-center mt-3">Login</h1>
                        <Form onSubmit ={event=> login(event)}>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control type="email" placeholder="Enter email" value={email} onChange={event =>{
                                setEmail(event.target.value)
                            }}/>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Password" value={password} onChange={event =>{
                                setPassword(event.target.value)
                            }}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicCheckbox">
                            </Form.Group>

                            <p>No account yet? Sign up <Link to = '/register'>here</Link></p>

                            <Button variant="primary" type="submit" disabled = {isDisabled}>
                                Login
                            </Button>
                        </Form>
                    </Col>
                </Row>
            </Container>

        : <Navigate to = '/*' />
    )
}