import { Container, Row, Col,  } from "react-bootstrap";
import { Link } from 'react-router-dom';

export default function Page404(){
    return(
        <Container>
            <Row>
                <Col>
                <h1>Page Not Found</h1>
                <p>Go to back to the <Link to = '/'>homepage</Link></p>
                
                </Col>
            </Row>
        </Container>
    )
}