import coursesData from "../data/courses"
import CourseCards from "../components/CourseCards"

import { useEffect, useState } from "react"


export default function Courses(){

const [courses, setCourses] = useState([])

useEffect(()=>{
    fetch(`${process.env.REACT_APP_API_URL}/courses/activeCourses`)
    .then(result=> result.json())
    .then(data =>{
        console.log(data)
        setCourses(data.map(course =>{
            return(
                <CourseCards key={course._id} courseProp = {course}/>
            )
        }))
    })
},[])


    //the prop that will be passed will be in  object data and the name of the prop will be the name of the attribute when passed

    //the map method loops through

   /* const courses = coursesData.map(course =>{
        return(
            <CourseCards key = {course.id} courseProp = {course} />
        )
    })
    */
    return(
        <>
            <h1 className="text-center mt-3" >Courses</h1>
           {courses}
        </>
    )
}