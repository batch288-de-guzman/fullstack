import { useState, useEffect } from "react";
import { Card, Container, Row, Col, Button } from "react-bootstrap";
import { useParams, useNavigate } from "react-router-dom";
import Swal2 from "sweetalert2";


export default function CourseView(){
    const[name, setName] = useState(``);
    const [description, setDescription] = useState(``)
    const [price, setPrice] = useState(``)

    const{ courseId } = useParams();
    const navigate = useNavigate();

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
        .then(result => result.json())
        .then( data=>{
            setName(data.name);
            setDescription(data.description)
            setPrice(data.price);
        })
    },[])

    const enroll = (courseId)=>{
        fetch(`${process.env.REACT_APP_API_URL}/user/enroll`,{
            method : 'POST',
            headers :   {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`,
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify({
                id: courseId
            })
        })
        .then(response => response.json())
        .then(data =>{
            console.log(data)
            if(data){
                Swal2.fire({
                    title: 'succesfully enrolled',
                    icon:'success',
                    text: 'you have successfully enrolled to this course'
                })
                navigate('/courses')
            }else{
                Swal2.fire({
                    title: 'something went wrong',
                    icon: 'error',
                    text: 'please try again'
                })
            }
        })
    }

    return(
        <Container>
            <Row>
                <Col className="mx-auto mt-3">
                    <Card>
                        <Card.Body>
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price</Card.Subtitle>
                            <Card.Text>{price}</Card.Text>
                            <Card.Subtitle>Class Schedule</Card.Subtitle>
                            <Card.Text>8AM - 5PM</Card.Text>
                            <Button variant="primary" onClick = {() => enroll(courseId)}>Enroll</Button>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}