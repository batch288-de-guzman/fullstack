import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import {useEffect, useContext } from "react"

export default function Logout(){

    //clear out the content of the local storage, use clear() method

    // localStorage.clear();
    const{setUser, unsetUser} = useContext(UserContext);

    useEffect(()=>{
        unsetUser();
        setUser({
            id: null,
            isAdmin: null
        })
    })


    return(
        <Navigate to = '/login'/>
    )
}