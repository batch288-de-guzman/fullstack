import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';




//createRoot() - assigns the element to be managed by its virtual dom 
const root = ReactDOM.createRoot(document.getElementById('root'));

//render() - displaying the component/react element in to the root
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
