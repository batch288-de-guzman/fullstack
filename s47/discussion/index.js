console.log(`sicko mode`);


//Document Object Model
    //allows access to modify property of an html element in a webpage
    //it is standard on how to get, change, add or delete HTML elements

    //using the querySelector it can access the html elements
        //css selectors to target specific elements
            //id selector(#)
            //class selector(.)
            //type selector(html tag)
            //universal selector (*)
            //attribut selector([attribute])

    //Query selectors has two type: querySelector nad querySelectorAll
// let firstElement = document.querySelector(`#txt-first-name`);
// console.log(firstElement);

    //query selector
    let secondElement = document.querySelector(`.full-name`);
    console.log(secondElement);
    //query selector all
    let thirdElement = document.querySelectorAll(`.full-name`)
    console.log(thirdElement);

    //getelements

    let element = document.getElementById(`fullName`)
 

    element = document.getElementsByClassName(`full-name`)
    console.log(element)

//Event Listener
    //whenever a user interacts with a webpage, this action is considered as an event
    //working with events is a large part of creating interactivity in a webpage

    //function `addEventListener`, takes two arguraments
        //furst arguments is a string indentifying the event
        //second, argument fuction that the event listeter will invoke

    let firstElement = document.querySelector
    (`#txt-first-name`);
    let lastName = document.querySelector(`#txt-last-name`);
    let fullName = document.querySelector(`#fullName`)


    firstElement.addEventListener(`keyup`,()=>{
        console.log(firstElement.value)
        fullName.innerHTML = `${firstElement.value}
        ${lastName.value}`
    });

 
    lastName.addEventListener(`keyup`,()=>{
        console.log(lastName.value)

        fullName.innerHTML = `${firstElement.value}
        ${lastName.value}`
    });

let textColor = document.querySelector(`#text-color`);
    console.log(textColor.value)

textColor.addEventListener(`change`,()=>{

    fullName.style.color = textColor.value;
})

    


    